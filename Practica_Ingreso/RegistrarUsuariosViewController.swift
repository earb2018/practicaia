//
//  RegistrarUsuariosViewController.swift
//  Practica_Ingreso
//
//  Created by Imangroup on 9/12/19.
//  Copyright © 2019 Imangroup. All rights reserved.
//

import UIKit
import CoreData

class RegistrarUsuariosViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var photoUserImage: UIImageView!
    @IBOutlet weak var nombreTxt: UITextField!
    @IBOutlet weak var correoTxt: UITextField!
    @IBOutlet weak var telTxt: UITextField!
    @IBOutlet weak var direccionTxt: UITextField!
    @IBOutlet weak var fechaNacimientoTxt: UITextField!
    var image = UIImage()
    
    private var datePicker : UIDatePicker?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        fechaNacimientoTxt.inputView = datePicker
        datePicker?.addTarget(self, action: #selector(self.cambioFecha(datePicker:)), for: .valueChanged)
        
    }
    
    @objc func cambioFecha(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        fechaNacimientoTxt.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
    }
    
    @IBAction func esconderTeclado(_ sender: UITapGestureRecognizer) {
        telTxt.resignFirstResponder()
        nombreTxt.resignFirstResponder()
        correoTxt.resignFirstResponder()
        direccionTxt.resignFirstResponder()
    }
    
    
    @IBAction func guardarUsuario(_ sender: UIButton) {
        if correoTxt.text!.isEmpty || direccionTxt.text!.isEmpty || fechaNacimientoTxt.text!.isEmpty || telTxt.text!.isEmpty || nombreTxt.text!.isEmpty {
            let alert = UIAlertController(title: "Aviso", message: "Por favor llenar todos los campos", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }else {
            let delegado = UIApplication.shared.delegate as! AppDelegate
            let contexto = delegado.persistentContainer.viewContext
            let entidad = NSEntityDescription.insertNewObject(forEntityName: "Usuario", into: contexto)
            
            entidad.setValue(correoTxt.text, forKey: "correo")
            entidad.setValue(direccionTxt.text, forKey: "direccion")
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd-MM-yyyy"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MM-yyyy"
            if let date = dateFormatterGet.date(from: fechaNacimientoTxt.text!) {
                
                entidad.setValue(date, forKey: "fechaNacimiento")
            } else {
                print("Error de formato en la fecha")
            }
            let telefono = String(telTxt.text!)
            entidad.setValue(telefono, forKey: "telefono")
            entidad.setValue(nombreTxt.text, forKey: "nombre")
            if image.isEqual(nil) {
                let imageData = UIImage(named: "user")!.pngData()
                entidad.setValue(imageData, forKey: "foto")
            }else {
                let imageData = image.pngData()
                entidad.setValue(imageData, forKey: "foto")
            }
            do {
                try contexto.save()
            } catch  {
                print("ERROR")
            }
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "mostrarUsuariosViewController") as! MostrarUsuariosViewController
            self.present(VC, animated: true, completion: nil)
            
        }
    }
    
    
    @IBAction func selectPhoto(_ sender: UIButton) {
        let imagePicker = UIImagePickerController()
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            imagePicker.sourceType = .camera
        }else {
            imagePicker.sourceType = .photoLibrary
        }
        
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    
}

extension RegistrarUsuariosViewController {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
         photoUserImage.image = image
        dismiss(animated: true, completion: nil)        
    }
}


extension RegistrarUsuariosViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
