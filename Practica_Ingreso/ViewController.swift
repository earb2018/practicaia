//
//  ViewController.swift
//  Practica_Ingreso
//
//  Created by Imangroup on 9/12/19.
//  Copyright © 2019 Imangroup. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var nombreUsuarioLable: UILabel!
    @IBOutlet weak var imagenUsuario: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    
    var usuarios = [NSManagedObject]()
    var registrado = false
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        obtenerDatos()
    }

    @IBAction func btnSiguiente(_ sender: UIButton) {
        if emailTextField.text!.isEmpty {
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "registrarUsuariosViewController") as! RegistrarUsuariosViewController
            self.present(VC, animated: true, completion: nil)
        }else{
            for user in usuarios {
                let correo = user.value(forKey: "correo") as! String
                if correo == emailTextField.text! {
                    registrado = true
                }
            }
            if registrado {
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "mostrarUsuariosViewController") as! MostrarUsuariosViewController
                self.present(VC, animated: true, completion: nil)            }else{
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "registrarUsuariosViewController") as! RegistrarUsuariosViewController
                self.present(VC, animated: true, completion: nil)            }
        }
    }
    
    func obtenerDatos() {
        // Obtener datos
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuario")        
        let delegado = UIApplication.shared.delegate as! AppDelegate
        let contexto = delegado.persistentContainer.viewContext
        request.returnsObjectsAsFaults = false
        do {
            usuarios = try contexto.fetch(request) as! [NSManagedObject]
            if usuarios.count != 0 {
                let lastUser = usuarios.last
                self.nombreUsuarioLable.text = lastUser?.value(forKey: "nombre") as? String
                self.emailTextField.text = lastUser?.value(forKey: "correo") as? String
                if let photo = lastUser?.value(forKey: "foto") {
                    let image = photo as! Data
                    self.imagenUsuario.image = UIImage(data: image)
                }else {
                    self.imagenUsuario.image = UIImage(named: "user")
                }
                
            }else {
                self.imagenUsuario.image = UIImage(named: "user")
            }
            
        } catch {
            print("Failed")
        }
        
        
    }
    
    func deleteAllData(entity: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedContext.delete(managedObjectData)
            }
        } catch let error as NSError {
            print("Delete all data in \(entity) error : \(error) \(error.userInfo)")
        }
    }
    
}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
