//
//  MostrarUsuariosViewController.swift
//  Practica_Ingreso
//
//  Created by Imangroup on 9/12/19.
//  Copyright © 2019 Imangroup. All rights reserved.
//

import UIKit
import CoreData

class celdaTablaUsuarios: UITableViewCell {
    @IBOutlet weak var btnDel: UIButton!
}

class MostrarUsuariosViewController: UIViewController {

    @IBOutlet weak var fotoUsuario: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var correoLabel: UILabel!
    @IBOutlet weak var telefono: UILabel!
    @IBOutlet weak var direcionLabel: UILabel!
    @IBOutlet weak var fechaNacimientoLabel: UILabel!
    @IBOutlet weak var usuarioLabel: UILabel!
    
    var usuarios = [NSManagedObject]()
    
    var usuarioSeleccionado = NSManagedObject()
    var indexSeleccionado = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.obtenerDatos()
        
        
    }
    
    func obtenerDatos() {
        // Obtener datos
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuario")
        let delegado = UIApplication.shared.delegate as! AppDelegate
        let contexto = delegado.persistentContainer.viewContext
        request.returnsObjectsAsFaults = false
        do {
            usuarios = try contexto.fetch(request) as! [NSManagedObject]
            if usuarios.isEmpty {
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "registrarUsuariosViewController") as! RegistrarUsuariosViewController
                self.present(VC, animated: true, completion: nil)
            } else {
                let firstUser = usuarios.last
                if let photo = firstUser?.value(forKey: "foto") {
                
                    let imagePNG = photo as! Data
                    self.fotoUsuario.image = UIImage(data: imagePNG)
                    
                } else {
                    self.fotoUsuario.image = UIImage(named: "user")
                }
                self.usuarioLabel.text = firstUser?.value(forKey: "nombre") as? String
                
                self.correoLabel.text = firstUser?.value(forKey: "correo") as? String
                self.telefono.text = firstUser?.value(forKey: "telefono") as? String
                self.direcionLabel.text = firstUser?.value(forKey: "direccion") as? String
                let fechaN = firstUser?.value(forKey: "fechaNacimiento") as! Date
                let format = DateFormatter()
                format.dateFormat = "dd-MM-yyyy"
                fechaNacimientoLabel.text = format.string(from: fechaN)            }
        } catch {
            print("Error al obtener datos")
        }
        
    }
    
    @IBAction func eliminarUsuario(_ sender: UIButton) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuario")
        let delegado = UIApplication.shared.delegate as! AppDelegate
        let contexto = delegado.persistentContainer.viewContext
        if (try? contexto.fetch(request)) != nil{
            contexto.delete(usuarioSeleccionado)
        }
        do {
            try contexto.save()
        } catch {
            print("Ya no hay elementos")
        }
        usuarios.remove(at: indexSeleccionado)
        obtenerDatos()
        self.tableView.reloadData()
        
    }
    
}




extension MostrarUsuariosViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.usuarios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath)
        let usuario = usuarios[indexPath.row]
        cell.textLabel?.text = usuario.value(forKey: "nombre") as! String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let cell = tableView.cellForRow(at: indexPath) as! celdaTablaUsuarios
        if cell.isSelected  {
            cell.btnDel.setImage(UIImage(named: "delete"), for: .normal)
            usuarioSeleccionado = usuarios[indexPath.row]
            indexSeleccionado = indexPath.row
            cell.isSelected = false
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! celdaTablaUsuarios
         cell.btnDel.setImage(UIImage(named: ""), for: .normal)
    }

    
    
}
